# Katawolf-core-model
model of katawolf's core

## Compile
```bash
mvn compile
```

## Package
```bash
mvn package
```

## Test
```bash
mvn test
```
